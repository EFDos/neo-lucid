# NeoLucid #

Research Data Collection Tool for a neuroscientific study, developed in C++ aiming multiple Desktop platforms.

### What is this repository for? ###

* This repository is aimed to hold and organize the Development of this tool
* 1.0.0

### How do I get set up? ###

* This software requires Gtkmm-3.0 libraries as well as updated SDL2 libraries.
* It should be built by the provided MakeFile or by using the CodeLite project.

### Contribution guidelines ###

* No outside contributions allowed atm.

### Who do I talk to? ###

* douglas.muratore@gmail.com
* contato@carlogaddi.com.br
