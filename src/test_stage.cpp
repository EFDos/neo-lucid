#include "test_stage.hpp"
#include "src/common/directory_reader.hpp"
#include "src/common/string_converter.hpp"

#include <random>
#include <algorithm>
#include <cstdio>

using namespace std;

namespace xaos
{

namespace neolucid
{

TestStage::TestStage(sf::RenderWindow& window)
: Stage(window),
interval_(0.f),
begun_(false),
state_control_(BEGIN),
current_round_(0),
max_rounds_(0),
current_sample_(0),
current_pair_(0),
feedback_frames_(0),
choice_(AdjList::BLANK)
{
    text1_.setFont(get_font());
    text2_.setFont(get_font());
    countdown_text_.setFont(get_font());
    
    countdown_text_.setString("Pressione qualquer tecla para iniciar o teste.");
    countdown_text_.setPosition(window.getSize().x/2 - 
        countdown_text_.getGlobalBounds().width/2, window.getSize().y/2 -
        countdown_text_.getGlobalBounds().height/2);
}

TestStage::~TestStage()
{
}

void TestStage::update_texts()
{
    text1_.setString(samples_[sample_order_[current_sample_]].list
        .get_pair(current_pair_ + current_round_ * 4).left);
    text2_.setString(samples_[sample_order_[current_sample_]].list
        .get_pair(current_pair_ + current_round_ * 4).right);
    text1_.setPosition(64, window_.getSize().y/2 -
    text1_.getGlobalBounds().height/2);

    text2_.setPosition(window_.getSize().x - 
    text2_.getGlobalBounds().width - 64,
        window_.getSize().y/2 - text2_.getGlobalBounds().height/2);
        
    text1_.setFillColor({255,255,255,255});
    text2_.setFillColor({255,255,255,255});
}

void TestStage::handle_events(sf::Event& event)
{
    if(event.type == sf::Event::Closed){
        finished_ = true;
    }
    switch(state_control_)
    {
        case BEGIN:
            if(!begun_){
                if(event.type == sf::Event::KeyReleased){
                    begun_ = true;
                    countdown_text_.setPosition(window_.getSize().x/2,
                        window_.getSize().y/2);
                    presentation_clock_.restart();
                }
            }
            break;
        case PRESENTATION:
            break;
        case EVALUATION:
            if(event.type == sf::Event::KeyReleased)
            {
                if(event.key.code == sf::Keyboard::A)
                {
                    samples_[sample_order_[current_sample_]].list
                    .get_pair(current_pair_ + current_round_ * 4).choice = AdjList::LEFT;
                    text1_.setFillColor({255,255,255,128});
                    choice_ = AdjList::LEFT;
                }
                else if(event.key.code == sf::Keyboard::L)
                {
                    samples_[sample_order_[current_sample_]].list
                    .get_pair(current_pair_ + current_round_ * 4).choice = AdjList::RIGHT;
                    text2_.setFillColor({255,255,255,128});
                    choice_ = AdjList::RIGHT;
                }
                else{
                    choice_ = AdjList::BLANK;
                    break;
                }
                
                state_control_ = EVAL_FEEDBACK;
            }
            break;
        case EVAL_FEEDBACK:
            break;
    }
}

void TestStage::update()
{
    
    if(!window_.isOpen()){
        finished_ = true;
        return;
    }
    
    window_.clear({60, 60, 60, 255});
    
    switch(state_control_)
    {
        case BEGIN:
            if(begun_)
            {
                if(presentation_clock_.getElapsedTime().asSeconds() < 3.0f)
                {
                    float t = presentation_clock_.getElapsedTime().asSeconds();
                    countdown_text_.setString(to_string(3 - (int)t));
                }
                else {
                    state_control_ = PRESENTATION;
                    presentation_clock_.restart();
                }
            }
            
            window_.draw(countdown_text_);
            break;
        case PRESENTATION:
            if(presentation_clock_.getElapsedTime().asSeconds() > interval_)
            {
                state_control_ = EVALUATION;
            }
            window_.draw(samples_[sample_order_[current_sample_]].image);
            break;
        case EVALUATION:
            window_.draw(text1_);
            window_.draw(text2_);
            break;
        case EVAL_FEEDBACK:
            if(++feedback_frames_ < 24){
                sf::Color alpha(255, 255, 255, 255 - (uint8)feedback_frames_*10);
                if(choice_ == AdjList::LEFT){
                    text1_.setFillColor(alpha);
                }
                else if(choice_ == AdjList::RIGHT){
                    text2_.setFillColor(alpha);
                }
                window_.draw(text1_);
                window_.draw(text2_);
            }
            else{
                feedback_frames_ = 0;
                
                if(++current_pair_ < 4)
                {
                    state_control_ = EVALUATION;
                }
                else
                {
                    current_pair_ = 0;
                    if(++current_sample_ < sample_order_.size())
                    {
                        state_control_ = PRESENTATION;
                        presentation_clock_.restart();
                    }
                    else
                    {
                        current_sample_ = 0;
                        int last_image = sample_order_.back();
                        
                        shuffle(sample_order_.begin(), sample_order_.end(),
                                default_random_engine());
                                
                        if(sample_order_[0] == last_image){
                            sample_order_[0] = sample_order_.back();
                            sample_order_.back() = last_image;
                        }
                                
                        if(++current_round_ < max_rounds_)
                        {
                            state_control_ = PRESENTATION;
                            presentation_clock_.restart();
                        }
                        else
                        {
                            finished_ = true;
                            return;
                        }
                    }
                }
                update_texts();
            }
            break;
    }
    
    window_.display();
}

void TestStage::set_adjectives(const vector<wstring>& adjs)
{
    adjs_ = adjs;
}

void TestStage::set_image_interval(double interval)
{
    interval_ = interval;
}

void TestStage::read_images_from_dir(const string& dir)
{
    auto files = read_dir(dir, FileFilter::PNG);
    
    int i = 0;
    for(auto& file : files)
    {        
        shared_ptr<sf::Texture> texture = make_shared<sf::Texture>();
        
        if(!texture->loadFromFile(dir + file)){
            wprintf(L"Error loading image: %s\n", string_to_wstring(dir+file));
        }
        
        textures_.push_back(texture);
        
        samples_.emplace_back();
        Sample& s = samples_.back();
        
        s.image.setTexture(*texture);
        s.image.setPosition(window_.getSize().x/2 -
            s.image.getGlobalBounds().width/2, 0);
        s.list.set_adjectives(adjs_);
        s.list.set_sample_name(L"Imagem" + to_wstring(i));
        s.list.randomize(samples_.size());
        
        sample_order_.push_back(i);
        ++i;
    }

    shuffle(sample_order_.begin(), sample_order_.end(),
            default_random_engine());
    
    max_rounds_ = samples_.back().list.size() / 4;
    
    update_texts();
}

vector<AdjList> TestStage::get_results() const
{
    vector<AdjList> list;
    
    for(auto& s : samples_)
    {
        list.push_back(s.list);
    }
    
    return list;
}

}

}
