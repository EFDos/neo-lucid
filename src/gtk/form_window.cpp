#include "../common/string_converter.hpp"
#include "form_window.hpp"

#include <fstream>

using namespace std;

namespace xaos
{

namespace gtk
{

FormWindow::FormWindow(int argc, char** argv)
: good_to_go_(false)
{
    app_ = Gtk::Application::create(argc, argv, "org.master.neolucid,base");
    
    #ifdef WIN32
    //Gtk::Settings settings;
    //settings.property_gtk_theme_name().set_value("win32");
    //app_->
    #endif
    
    
    builder_ = Gtk::Builder::create_from_file("res/neolucid.glade");
    
    builder_->get_widget("main_window", window_);
    
    builder_->get_widget("start_button", start_button_);
    
    start_button_->signal_clicked()
        .connect(sigc::mem_fun(*this, &FormWindow::on_start_clicked));
    
    window_->set_default_size(800, 1024);
}

FormWindow::~FormWindow()
{
}

int FormWindow::run()
{
    return app_->run(*window_);
}

void FormWindow::on_start_clicked()
{
    ofstream file;
    Gtk::Entry* name_entry;
    
    builder_->get_widget("name_entry", name_entry);
    
    Glib::ustring name = name_entry->get_text();
    
    if(name.empty()){
        Gtk::MessageDialog dialog(*window_, "Formulário mal preenchido");
        dialog.set_secondary_text("Nome é um campo obrigatório.");
        dialog.run();
        return;
    }

    Glib::ustring lower_name = name.lowercase();
    Glib::ustring file_name("results/" + lower_name + ".csv");
    
    subj_name_ = lower_name.raw();
    
    file.open(file_name);
    file << "Nome;" << name << endl;
    
    if(!file.is_open()){
        return;
    }
    
    {
        Gtk::SpinButton* age_spin;
        builder_->get_widget("age_spin", age_spin);
        
        file << "Idade;" << age_spin->get_value_as_int() << endl;
    }
    
    {
        Gtk::Entry* gender_entry;
        builder_->get_widget("gender_entry", gender_entry);
        
        if(gender_entry->get_text().empty()){
            //error
            Gtk::MessageDialog dialog(*window_, "Formulário mal preenchido");
            dialog.set_secondary_text("Sexo é um campo obrigatório.");
            dialog.run();
            file.close();
            remove(file_name.c_str());
            return;
        }
        
        file << "Sexo;" << gender_entry->get_text() << endl;
    }
    
    {
        Gtk::Entry* email_entry;
        builder_->get_widget("email_entry", email_entry);
        
        if(!email_entry->get_text().empty()){
            file << "E-mail;" << email_entry->get_text() << endl;
        }
    }
    
    {
        Gtk::CheckButton* highschool_check;
        builder_->get_widget("highschool_check", highschool_check);
        
        if(highschool_check->get_active()){
            file << "Ensino médio;Concluído" << endl;
        }
        else{
            file << "Ensino médio;Não concluído" << endl;
        }
    }
    
    {
        Gtk::Entry* grad_entry;
        Gtk::Entry* grad_place_entry;
        
        builder_->get_widget("grad_entry", grad_entry);
        builder_->get_widget("grad_place_entry", grad_place_entry);
        
        if(!grad_entry->get_text().empty())
        {
            file << "Graduado em;" << grad_entry->get_text();
            
            if(!grad_place_entry->get_text().empty()){
                file << ";" << grad_place_entry->get_text() << endl;
            }
            else{
                file << endl;
            }
        }
    }
    
    {
        Gtk::Entry* tec_entry;
        Gtk::Entry* tec_place_entry;
        
        builder_->get_widget("tec_entry", tec_entry);
        builder_->get_widget("tec_place_entry", tec_place_entry);
        
        if(!tec_entry->get_text().empty())
        {
            file << "Técnico em;" << tec_entry->get_text();
            
            if(!tec_place_entry->get_text().empty()){
                file << ";" << tec_place_entry->get_text() << endl;
            }
            else{
                file << endl;
            }
        }
    }
    
    {
        Gtk::Entry* lato_sensu_entry;
        Gtk::Entry* lato_sensu_place_entry;
        
        builder_->get_widget("lato_sensu_entry", lato_sensu_entry);
        builder_->get_widget("lato_sensu_place_entry", lato_sensu_place_entry);
        
        if(!lato_sensu_entry->get_text().empty())
        {
            file << "Lato sensu em;" << lato_sensu_entry->get_text();
            
            if(!lato_sensu_place_entry->get_text().empty()){
                file << ";" << lato_sensu_place_entry->get_text() << endl;
            }
            else{
                file << endl;
            }
        }
    }
    
    {
        Gtk::Entry* master_entry;
        Gtk::Entry* master_place_entry;
        
        builder_->get_widget("master_entry", master_entry);
        builder_->get_widget("master_place_entry", master_place_entry);
        
        if(!master_entry->get_text().empty())
        {
            file << "Mestre em;" << master_entry->get_text();
            
            if(!master_place_entry->get_text().empty()){
                file << ";" << master_place_entry->get_text() << endl;
            }
            else{
                file << endl;
            }
        }
    }
    
    {
        Gtk::Entry* doctor_entry;
        Gtk::Entry* doctor_place_entry;
        
        builder_->get_widget("doctor_entry", doctor_entry);
        builder_->get_widget("doctor_place_entry", doctor_place_entry);
        
        if(!doctor_entry->get_text().empty())
        {
            file << "Doutor em;" << doctor_entry->get_text();
            
            if(!doctor_place_entry->get_text().empty()){
                file << ";" << doctor_place_entry->get_text() << endl;
            }
            else{
                file << endl;
            }
        }
    }
    
    {
        Gtk::Entry* on_grad_entry;
        Gtk::Entry* on_grad_place_entry;
        
        builder_->get_widget("on_grad_entry", on_grad_entry);
        builder_->get_widget("on_grad_place_entry", on_grad_place_entry);
        
        if(!on_grad_entry->get_text().empty())
        {
            file << "Graduando em;" << on_grad_entry->get_text();
            
            if(!on_grad_place_entry->get_text().empty()){
                file << ";" << on_grad_place_entry->get_text() << endl;
            }
            else{
                file << endl;
            }
        }
    }
    
    {
        Gtk::Entry* on_tec_entry;
        Gtk::Entry* on_tec_place_entry;
        
        builder_->get_widget("on_tec_entry", on_tec_entry);
        builder_->get_widget("on_tec_place_entry", on_tec_place_entry);
        
        if(!on_tec_entry->get_text().empty())
        {
            file << "Cursando técnico em;" << on_tec_entry->get_text();
            
            if(!on_tec_place_entry->get_text().empty()){
                file << ";" << on_tec_place_entry->get_text() << endl;
            }
            else{
                file << endl;
            }
        }
    }
    
    {
        Gtk::Entry* on_lato_sensu_entry;
        Gtk::Entry* on_lato_sensu_place_entry;
        
        builder_->get_widget("on_lato_sensu_entry", on_lato_sensu_entry);
        builder_->get_widget("on_lato_sensu_place_entry", on_lato_sensu_place_entry);
        
        if(!on_lato_sensu_entry->get_text().empty())
        {
            file << "Realizando lato sensu em;" << on_lato_sensu_entry->get_text();
            
            if(!on_lato_sensu_place_entry->get_text().empty()){
                file << ";" << on_lato_sensu_place_entry->get_text() << endl;
            }
            else{
                file << endl;
            }
        }
    }
    
    {
        Gtk::Entry* on_master_entry;
        Gtk::Entry* on_master_place_entry;
        
        builder_->get_widget("on_master_entry", on_master_entry);
        builder_->get_widget("on_master_place_entry", on_master_place_entry);
        
        if(!on_master_entry->get_text().empty())
        {
            file << "Mestrando em;" << on_master_entry->get_text();
            
            if(!on_master_place_entry->get_text().empty()){
                file << ";" << on_master_place_entry->get_text() << endl;
            }
            else{
                file << endl;
            }
        }
    }
    
    {
        Gtk::Entry* on_doctor_entry;
        Gtk::Entry* on_doctor_place_entry;
        
        builder_->get_widget("on_doctor_entry", on_doctor_entry);
        builder_->get_widget("on_doctor_place_entry", on_doctor_place_entry);
        
        if(!on_doctor_entry->get_text().empty())
        {
            file << "Doutorando em;" << on_doctor_entry->get_text();
            
            if(!on_doctor_place_entry->get_text().empty()){
                file << ";" << on_doctor_place_entry->get_text() << endl;
            }
            else{
                file << endl;
            }
        }
    }
    
    {
        Gtk::Entry* profession_entry;
        builder_->get_widget("profession_entry", profession_entry);
        
        if(profession_entry->get_text().empty()){            
            Gtk::MessageDialog dialog(*window_, "Formulário mal preenchido");
            dialog.set_secondary_text("Profissão é um campo obrigatório.");
            dialog.run();
            file.close();
            remove(file_name.c_str());
            return;
        }
        else{
            file << "Profissão;" << profession_entry->get_text() << endl;
        }
    }
    
    {
        Gtk::RadioButton* radio_artwork_yes;
        builder_->get_widget("radio_artwork_yes", radio_artwork_yes);
        
        
        file << "Trabalho relacionado à artes visuais;";
        if(radio_artwork_yes->get_active())
        {
            
            Gtk::Entry* artwork_entry;
            Gtk::SpinButton* artwork_duration_spin;
            builder_->get_widget("artwork_entry", artwork_entry);
            builder_->get_widget("artwork_duration_spin", artwork_duration_spin);
            
            if(artwork_entry->get_text().empty()){
                Gtk::MessageDialog dialog(*window_, "Formulário mal preenchido");
                dialog.set_secondary_text("Foi marcado que o usuário trabalhou"
                                          " em alguma área relacionada à artes"
                                          " visuais, porém o campo não foi"
                                          " preenchido.");
                dialog.run();
                file.close();
                remove(file_name.c_str());
                return;
            }
            else{
                file << artwork_entry->get_text() << endl;
                file << "Duração;" << artwork_duration_spin->get_value_as_int()
                << endl;
            }
        }
        else{
            file << "Não" << endl;
        }
    }

    {
        Gtk::Scale* art_interest_scale;
        builder_->get_widget("art_interest_scale", art_interest_scale);
        
        file << "Interesse em artes;" << art_interest_scale->get_value() << endl;
    }

    {
        Gtk::RadioButton*           radio_artexpo_no;
        Gtk::RadioButton*           radio_artexpo_12;
        Gtk::RadioButton*           radio_artexpo_34;
        Gtk::RadioButton*           radio_artexpo_56;
        Gtk::RadioButton*           radio_artexpo_7p;
        
        builder_->get_widget("radio_artexpo_no", radio_artexpo_no);
        builder_->get_widget("radio_artexpo_12", radio_artexpo_12);
        builder_->get_widget("radio_artexpo_34", radio_artexpo_34);
        builder_->get_widget("radio_artexpo_56", radio_artexpo_56);
        builder_->get_widget("radio_artexpo_7p", radio_artexpo_7p);
        
        file << "Costuma ir à exposições de obras de artes;";
        
        if(radio_artexpo_no->get_active()){
            file << "Não" << endl;
        }
        if(radio_artexpo_12->get_active()){
            file << "1 a 2" << endl;
        }
        if(radio_artexpo_34->get_active()){
            file << "3 a 4" << endl;
        }
        if(radio_artexpo_56->get_active()){
            file << "5 a 6" << endl;
        }
        if(radio_artexpo_7p->get_active()){
            file << "7 a mais" << endl;
        }
    }

    {
        Gtk::RadioButton*   radio_figurative;
        builder_->get_widget("radio_figurative", radio_figurative);
        
        if(radio_figurative->get_active()){
            file << "Tipo de obra favorita;" << "Figurativa" << endl;
        }
        else{
            file << "Tipo de obra favorita;" << "Abstrata" << endl;            
        }
    }
    
    {
        Gtk::Scale* art_knowledge_scale;
        builder_->get_widget("art_knowledge_scale", art_knowledge_scale);
        
        file << "Conhecimento de artes;" << art_knowledge_scale->get_value()
            << endl;
    }

    {
        Gtk::CheckButton*   eyedis_col_check;
        Gtk::CheckButton*   eyedis_cone_check;
        Gtk::CheckButton*   eyedis_glauc_check;
        Gtk::CheckButton*   eyedis_ret_check;
        Gtk::Entry*         eyedis_entry;
        
        builder_->get_widget("eyedis_col_check", eyedis_col_check);
        builder_->get_widget("eyedis_cone_check", eyedis_cone_check);
        builder_->get_widget("eyedis_glauc_check", eyedis_glauc_check);
        builder_->get_widget("eyedis_ret_check", eyedis_ret_check);
        builder_->get_widget("eyedis_entry", eyedis_entry);
        
        file << "Doenças oculares:" << endl;
        if(eyedis_col_check->get_active()){
            file << "Alteração de visão de cores" << endl;
        }
        if(eyedis_cone_check->get_active()){
            file << "Ceratocone" << endl;
        }
        if(eyedis_glauc_check->get_active()){
            file << "Glaucoma" << endl;
        }
        if(eyedis_ret_check->get_active()){
            file << "Doença na retina" << endl;
        }
        if(!eyedis_entry->get_text().empty()){
            file << "Outras;" << eyedis_entry->get_text() << endl;
        }
    }
    
    {
        Gtk::CheckButton*   mentaldis_depress_check;
        Gtk::CheckButton*   mentaldis_anx_check;
        Gtk::CheckButton*   mentaldis_bipolar_check;
        Gtk::CheckButton*   mentaldis_dem_check;
        Gtk::CheckButton*   mentaldis_dda_check;
        Gtk::CheckButton*   mentaldis_schizo_check;
        Gtk::CheckButton*   mentaldis_obsess_check;
        Gtk::CheckButton*   mentaldis_autism_check;
        Gtk::CheckButton*   mentaldis_border_check;
        Gtk::Entry*         mentaldis_entry;
        
        builder_->get_widget("mentaldis_depress_check", mentaldis_depress_check);
        builder_->get_widget("mentaldis_anx_check", mentaldis_anx_check);
        builder_->get_widget("mentaldis_bipolar_check", mentaldis_bipolar_check);
        builder_->get_widget("mentaldis_dem_check", mentaldis_dem_check);
        builder_->get_widget("mentaldis_dda_check", mentaldis_dda_check);
        builder_->get_widget("mentaldis_schizo_check", mentaldis_schizo_check);
        builder_->get_widget("mentaldis_obsess_check", mentaldis_obsess_check);
        builder_->get_widget("mentaldis_autism_check", mentaldis_autism_check);
        builder_->get_widget("mentaldis_border_check", mentaldis_border_check);
        builder_->get_widget("mentaldis_entry", mentaldis_entry);
        
        file << "Transtornos mentais:" << endl;
        
        if(mentaldis_depress_check->get_active()){
            file << "Depressão" << endl;
        }
        if(mentaldis_anx_check->get_active()){
            file << "Ansiedade" << endl;
        }
        if(mentaldis_bipolar_check->get_active()){
            file << "Bipolaridade" << endl;
        }
        if(mentaldis_dem_check->get_active()){
            file << "Demência" << endl;
        }
        if(mentaldis_dda_check->get_active()){
            file << "Déficit de atenção" << endl;
        }
        if(mentaldis_schizo_check->get_active()){
            file << "Esquizofrenia" << endl;
        }
        if(mentaldis_obsess_check->get_active()){
            file << "Transtorno obsessivo-compulsivo" << endl;
        }
        if(mentaldis_autism_check->get_active()){
            file << "Autismo" << endl;
        }
        if(mentaldis_border_check->get_active()){
            file << "Borderline" << endl;
        }
        if(!mentaldis_entry->get_text().empty()){
            file << "Outros;" << mentaldis_entry->get_text() << endl;
        }
    }
    
    {
        Gtk::CheckButton*   med_fluox_check;
        Gtk::CheckButton*   med_esc_check;
        Gtk::CheckButton*   med_depak_check;
        Gtk::CheckButton*   med_bupro_check;
        Gtk::CheckButton*   med_litium_check;
        Gtk::CheckButton*   med_rivo_check;
        Gtk::CheckButton*   med_lex_check;
        Gtk::CheckButton*   med_parox_check;
        Gtk::CheckButton*   med_sertra_check;
        Gtk::CheckButton*   med_rita_check;
        Gtk::Entry*         med_entry;
        
        builder_->get_widget("med_fluox_check", med_fluox_check);
        builder_->get_widget("med_esc_check", med_esc_check);
        builder_->get_widget("med_depak_check", med_depak_check);
        builder_->get_widget("med_bupro_check", med_bupro_check);
        builder_->get_widget("med_litium_check", med_litium_check);
        builder_->get_widget("med_rivo_check", med_rivo_check);
        builder_->get_widget("med_lex_check", med_lex_check);
        builder_->get_widget("med_parox_check", med_parox_check);
        builder_->get_widget("med_sertra_check", med_sertra_check);
        builder_->get_widget("med_rita_check", med_rita_check);
        builder_->get_widget("med_entry", med_entry);
        
        file << "Medicamentos:" << endl;
        
        if(med_fluox_check->get_active()){
            file << "Fluoxetina" << endl;
        }
        if(med_esc_check->get_active()){
            file << "Escitalopram" << endl;
        }
        if(med_depak_check->get_active()){
            file << "Depakote" << endl;
        }
        if(med_bupro_check->get_active()){
            file << "Bupropiona" << endl;
        }
        if(med_litium_check->get_active()){
            file << "Lítio" << endl;
        }
        if(med_rivo_check->get_active()){
            file << "Rivotril" << endl;
        }
        if(med_lex_check->get_active()){
            file << "Lexotan" << endl;
        }
        if(med_parox_check->get_active()){
            file << "Paroxetina" << endl;
        }
        if(med_sertra_check->get_active()){
            file << "Sertralina" << endl;
        }
        if(med_rita_check->get_active()){
            file << "Ritalina" << endl;
        }
        if(!med_entry->get_text().empty()){
            file << "Outros;" << med_entry->get_text() << endl;
        }
    }

    good_to_go_ = true;

    app_->quit();
}

bool FormWindow::was_successful()
{
    return good_to_go_;
}

const string& FormWindow::get_subject_name()
{
    return subj_name_;
}

}//gtk

}//xaos
