#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <gtkmm-3.0/gtkmm.h>

namespace xaos
{

namespace gtk
{

class FormWindow
{
    
    Glib::RefPtr<Gtk::Builder>      builder_;
    Glib::RefPtr<Gtk::Application>  app_;
    
    Gtk::Button*                    start_button_;
    Gtk::Window*                    window_;
    
    std::string                    subj_name_;
    
    bool                            good_to_go_;
    
    void on_start_clicked();
    
public:
    
    FormWindow(int argc, char** argv);
    
    ~FormWindow();
    
    int run();
    
    bool was_successful();
    
    const std::string& get_subject_name();
    
};

}//gtk

}//xaos

#endif // MAINWINDOW_HPP
