#ifndef TESTSTAGE_HPP
#define TESTSTAGE_HPP

#include "stage.hpp"
#include "adj_list.hpp"

#include <vector>
#include <memory>

namespace xaos
{

namespace neolucid
{

class TestStage : public Stage
{
    
    sf::Text                    text1_;
    sf::Text                    text2_;
    sf::Text                    countdown_text_;
    
    std::vector<std::wstring>   adjs_;
    double                       interval_;
    
    enum States {
        BEGIN,
        PRESENTATION,
        EVALUATION,
        EVAL_FEEDBACK
    };
    
    bool                        begun_;
    unsigned int                state_control_;
    unsigned int                current_round_;
    unsigned int                max_rounds_;
    unsigned int                current_sample_;
    unsigned int                current_pair_;
    unsigned int                feedback_frames_;
    uint8                       choice_;
    sf::Clock                   presentation_clock_;
    
    struct Sample {
        sf::Sprite  image;
        AdjList     list;
    };
    
    std::vector<Sample>                             samples_;
    std::vector<std::shared_ptr<sf::Texture>>       textures_;
    std::vector<int>                                sample_order_;
    
    void update_texts();
    
public:
    TestStage(sf::RenderWindow&);
    
    virtual ~TestStage();
    
    void handle_events(sf::Event&) override;
    
    void update() override;
    
    void set_adjectives(const std::vector<std::wstring>& adjs);
    
    void read_images_from_dir(const std::string& dir);
    
    void set_image_interval(double interval);
    
    std::vector<AdjList> get_results() const;
};

}

}

#endif // TESTSTAGE_HPP
