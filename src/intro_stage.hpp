#ifndef INTROSTAGE_HPP
#define INTROSTAGE_HPP

#include "stage.hpp"

namespace xaos
{

namespace neolucid
{

class IntroStage : public Stage
{
    
    sf::Text   text;
    int         frame_counter;
    
public:
    IntroStage(sf::RenderWindow&);
    
    virtual ~IntroStage();

    void handle_events(sf::Event&) override;
    
    void update() override;
};

}

}

#endif // INTROSTAGE_HPP
