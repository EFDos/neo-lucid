#include "adj_list.hpp"

#include <iostream>
#include <algorithm>
#include <random>

using namespace std;

namespace xaos
{

namespace neolucid
{

AdjList::AdjList()
{
}

AdjList::AdjList(const vector<wstring>& adjs)
{
    generate_combinations(adjs);
}

AdjList::AdjList(const AdjList& list)
: list_(list.list_),
  sample_name_(list.sample_name_)
{
}

AdjList::AdjList(AdjList&& list)
: list_(move(list.list_)),
  sample_name_(move(list.sample_name_))
{
}

AdjList::~AdjList()
{
}

void AdjList::generate_combinations(const vector<wstring>& adjs)
{
    list_.clear();
    
    for(size_t i = 0 ; i < adjs.size() ; ++i)
    {
        for(size_t j = 0 ; j < adjs.size() ; ++j)
        {
            if(i == j)
                continue;
            
            list_.push_back(AdjPair(adjs[i], adjs[j]));
        }
    }
}

int AdjList::ordering_rule_check()
{
    int     n_calls = 1;
    bool    recheck = false;
    
    for(size_t i = 0 ; i < list_.size() ; ++i)
    {
        const wstring& left  = list_[i].left;
        const wstring& right = list_[i].right;
        
        for(size_t j = i ; j <= (i + 3) && j < list_.size() ; ++j)
        {
            if(list_[j].left == right && list_[j].right == left)
            {
                AdjPair temp(list_[j]);
                list_.erase(list_.begin() + j);
                
                if(i < list_.size() / 2){
                    list_.push_back(move(temp));
                }
                else{
                    list_.insert(list_.begin(), 1, move(temp));
                }
                recheck = true;
            }
        }
    }
    
    if(recheck){
        n_calls += ordering_rule_check();
    }
    
    return n_calls;
}

void AdjList::set_adjectives(const vector<wstring>& adjs)
{
    generate_combinations(adjs);
}

void AdjList::set_sample_name(const wstring& name)
{
    sample_name_ = name;
}

void AdjList::set_sample_name(wstring&& name)
{
    sample_name_ = name;
}

void AdjList::randomize(int32 seed)
{
    if(list_.empty()){
        wcerr << L"error<xaos::neolucid::List::randomize()>:"
                 L"Can't randomize empty list." << endl;
        return;
    }
    
    default_random_engine rd;
    rd.seed(seed);
    shuffle(list_.begin(), list_.end(), rd);
    
    int calls = ordering_rule_check();
    
    wcout << L"info<xaos::neolucid::List::ordering_rule_check()>:"
             L"Valid ordering achieved in " << calls << " calls." << endl;
}

AdjList::AdjPair& AdjList::get_pair(size_t index)
{
    try{
        return list_[index];
    }
    catch(out_of_range& e){
        wcerr << L"error<xaos::neolucid::List::get_pair()>: out of bounds"
              << e.what() << endl;
    }
}

const wstring& AdjList::get_sample_name() const
{
    return sample_name_;
}

const vector<AdjList::AdjPair>& AdjList::peek_list() const
{
    return list_;
}

size_t AdjList::size() const
{
    return list_.size();
}

}

}