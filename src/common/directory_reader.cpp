#include "directory_reader.hpp"
#include <cstdio>
#include <dirent.h>

using namespace std;

namespace xaos
{

vector<string> read_dir(const string& path, FileFilter filter)
{
    vector<string> files;
    
    DIR* dir = opendir(path.c_str());
    
    if(dir == nullptr){
        printf("Error reading directory %s\n", path.c_str());
        return files;
    }
    
    dirent* ent;
    
    while((ent = readdir(dir)) != nullptr)
    {
        #ifndef WIN32
        if(ent->d_type != DT_REG){
            continue;
        }
        #endif

        string file(ent->d_name);
        
        if(filter == FileFilter::UNDEF)
        {
            files.push_back(file);
            continue;
        }
        else
        {
            size_t dot = file.find_last_of('.');
            if(dot == string::npos){
                continue;
            }
            string ext(file.substr(dot, file.length() - dot));
            switch(filter)
            {
                case FileFilter::UNDEF:
                    break;
                case FileFilter::PNG:
                    if(ext == ".png"){
                        files.push_back(file);
                    }
                    break;
                case FileFilter::TXT:
                    if(ext == ".txt"){
                        files.push_back(file);
                    }
                    break;
                case FileFilter::TOML:
                    if(ext == ".toml"){
                        files.push_back(file);
                    }
                    break;
                default:
                    break;
            }
        }
    }
    closedir(dir);
    
    return files;
}

}