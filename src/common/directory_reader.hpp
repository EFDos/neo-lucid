#ifndef DIRECTORY_READER
#define DIRECTORY_READER

#include <vector>
#include <string>

namespace xaos
{

enum class FileFilter
{
    UNDEF = 0,
    PNG,
    TXT,
    TOML
};


std::vector<std::string> read_dir(const std::string& path,
    FileFilter = FileFilter::UNDEF);

}

#endif