#ifndef STR_CONVERTER_HPP
#define STR_CONVERTER_HPP

#include <string>
#include <locale>
#include <codecvt>

namespace xaos
{
    
inline std::wstring string_to_wstring(const std::string& str)
{
    std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
    return converter.from_bytes(str);
}

inline std::string wstring_to_string(const std::wstring& wstr)
{
    std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
    return converter.to_bytes(wstr);
}
    
}

#endif