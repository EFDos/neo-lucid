#ifndef COLOR_HPP
#define COLOR_HPP

#include "int.hpp"

namespace xaos
{

struct Color
{
    uint8 r;
    uint8 g;
    uint8 b;
    uint8 a;
    
    Color(uint8 _r = 255, uint8 _g = 255, uint8 _b = 255, uint8 _a = 255)
    : r(_r), g(_g), b(_b), a(_a) {}
    
    Color(const Color& cp)
    : r(cp.r),
      g(cp.g),
      b(cp.b),
      a(cp.a) {}
      
    Color& operator =(const Color& cp)
    {
        r = cp.r;
        g = cp.g;
        b = cp.b;
        a = cp.a;
        
        return *this;
    }
};

}

#endif //COLOR_HPP