#include "gtk/form_window.hpp"
#include "tutorial_stage.hpp"
#include "intro_stage.hpp"
#include "test_stage.hpp"
#include "common/string_converter.hpp"

#include <locale>
#include <fstream>
#include <iostream>

void run_stage(xaos::neolucid::Stage&);

void write_results(const std::string&,
    const std::vector<xaos::neolucid::AdjList>&);

void read_config(std::vector<std::wstring>&, double&);

int main(int argc, char** argv)
{
    std::locale::global(std::locale(""));
    
    std::vector<std::wstring>   adjs;
    double                      interval;
    std::string                subj_name;
    
    {
        xaos::gtk::FormWindow form_window(argc, argv);
    
        int ret = form_window.run();
    
        if(!form_window.was_successful()){
            return ret;
        }
        
        subj_name = form_window.get_subject_name();
    }
    
    
    read_config(adjs, interval);
    
    sf::ContextSettings context;
    sf::RenderWindow window(sf::VideoMode::getDesktopMode(),
        "NeoLucid", sf::Style::Fullscreen);
    window.setVerticalSyncEnabled(true);
    window.setMouseCursorVisible(false);

    {
        xaos::neolucid::TutorialStage tutorial(window);
        xaos::neolucid::IntroStage  intro(window);
        xaos::neolucid::TestStage   test(window);
        
        test.set_adjectives(std::move(adjs));
        test.set_image_interval(interval);
        test.read_images_from_dir("res/");
        
        run_stage(intro);
        run_stage(tutorial);
        run_stage(test);

        write_results(subj_name, test.get_results());
    }
    
    return 0;
}

void read_config(std::vector<std::wstring>& adjs, double& interval)
{
    std::wifstream  file("res/config.txt");
    std::wstring    line;
    
    while(std::getline(file, line))
    {
        std::wstring key;
        if(!line.empty())
        {
            key = line.substr(0, line.find('=') - 1);
            key.erase(std::remove_if(key.begin(), key.end(), std::iswspace),
                key.end());
            
            if(key == L"adjetivos")
            {
                size_t pos = line.find(L'\"') + 1;
                while(pos != std::wstring::npos && pos != 0)
                {
                    std::wstring value = line.substr(pos, 
                        (line.find(L'\"', pos)) - pos);
                        
                    adjs.push_back(std::move(value));
                    
                    pos = line.find(L'\"', pos) + 1;
                    pos = line.find(L'\"', pos) + 1;
                }
            }
            if(key == L"intervalo")
            {
                std::wstring value;
                value = line.substr(line.find('=') + 1, line.length());
                interval = std::stod(value);
            }
        }
    }
}

void run_stage(xaos::neolucid::Stage& stage)
{
    while(!stage.finished()){
        stage.poll_events();
        stage.update();
    }
}

void write_results(const std::string& name, 
    const std::vector<xaos::neolucid::AdjList>& lists)
{
    std::wofstream file("results/" + name + "_sample.csv");
    
    #ifdef WIN32
    std::locale loc(std::locale::classic(), new std::codecvt_utf8<wchar_t>);
    file.imbue(loc);
    #endif
    
    if(!file.is_open())
    {
        std::wcout << L"Error opening/creating file results/" + xaos::string_to_wstring(name) + 
                        L"_sample.csv\n";
        return;
    }
    
    for(auto& list : lists)
    {
        file << list.get_sample_name() << L",,,,";
        
        file << std::endl;
        
        for(auto& pair : list.peek_list())
        {
            if(pair.choice == xaos::neolucid::AdjList::LEFT){
                file << pair.left << L",1," << pair.right << L",0,";
            }
            if(pair.choice == xaos::neolucid::AdjList::RIGHT){
                file << pair.left << L",0," << pair.right << L",1,";
            }
            else if(pair.choice == xaos::neolucid::AdjList::BLANK){
                file << pair.left << L",," << pair.right << L",,";
            }
            
            file << std::endl;
        }
    }
}