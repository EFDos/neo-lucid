#ifndef ADJLIST_HPP
#define ADJLIST_HPP

#include "src/common/int.hpp"

#include <string>
#include <vector>

namespace xaos
{

namespace neolucid
{

class AdjList
{
public:

    enum {
        BLANK   = -1,
        LEFT    = 0,    //We'll use 0 and 1 for determining wether left or
        RIGHT   = 1     //right was chosen
    };
    
    
    struct AdjPair {
        std::wstring left;
        std::wstring right;
        int8         choice;
        
        AdjPair(const std::wstring& l, const std::wstring& r)
        : left(l), right(r), choice(BLANK) {}
    };
    
private:

    std::vector<AdjPair>    list_;
    std::wstring            sample_name_;
    
    void generate_combinations(const std::vector<std::wstring>& adjs);
    int ordering_rule_check();
    
public:
    
    AdjList();
    AdjList(const std::vector<std::wstring>& adjs);
    AdjList(const AdjList& list);
    AdjList(AdjList&& list);
    ~AdjList();
    
    void set_adjectives(const std::vector<std::wstring>& adjs);
    
    void set_sample_name(const std::wstring& name);
    
    void set_sample_name(std::wstring&& name);
    
    void randomize(int32 seed);
    
    AdjPair& get_pair(size_t index);
    
    const std::wstring& get_sample_name() const;
    
    const std::vector<AdjPair>& peek_list() const;
    
    size_t size() const;

};

}

}

#endif // ADJLIST_HPP
