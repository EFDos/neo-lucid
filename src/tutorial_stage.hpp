#ifndef TUTORIALSTAGE_HPP
#define TUTORIALSTAGE_HPP

#include "stage.hpp"

namespace xaos
{

namespace neolucid
{

class TutorialStage : public Stage
{
    
    sf::Text        text1_, text2_;
    sf::Sprite      sample_image_;
    sf::Texture     sample_image_tex_;
    
    enum {
        LEFT = 0,
        RIGHT
    };
    
    bool            feedback_;
    int             feedback_frames_;
    int             end_frames_;
    int             choice_;
    int             state_control_;
    sf::Clock       prsnt_clock_;
    
    enum States {
        EXPL_TEXT = 0,
        IMG_PRSNT,
        ADJ_PAIR1,
        ADJ_PAIR2,
        ADJ_PAIR3,
        ADJ_PAIR4,
        TUT_END,
        MAX_STATES
    };
    
    void update_texts();
    
public:

    TutorialStage(sf::RenderWindow& window);
    
    virtual ~TutorialStage();
    
    void handle_events(sf::Event&) override;
    
    void update() override;
};

}

}

#endif // TUTORIALSTAGE_HPP
