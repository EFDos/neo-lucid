#include "tutorial_stage.hpp"
#include "src/common/int.hpp"

namespace xaos
{

namespace neolucid

{

TutorialStage::TutorialStage(sf::RenderWindow& window)
:   Stage(window),
    feedback_(false),
    feedback_frames_(0),
    end_frames_(0),
    choice_(-1),
    state_control_(0)
{
    sample_image_tex_.loadFromFile("res/sample/sample.png");
    
    text1_.setFont(get_font());
    text2_.setFont(get_font());
    sample_image_.setTexture(sample_image_tex_);
    
    text1_.setString(L" Tutorial\n\n"
                                    " Visualize atentamente a imagem.\n"
                                    " Selecione os adjetivos que melhor a represente.\n"
                                    " Utilize a tecla A para selecionar o adjetivo da esquerda\n"
                                    " e a tecla L para selecionar o adjetivo da direita.\n"
                                    " O tutorial irá rodar uma amostra de exemplo e o teste\n"
                                    " terá início logo em seguida.\n"
                                    " O teste levará aproximadamente 20 minutos.\n\n"
                                    " Pressione qualquer tecla para continuar."); 
    text1_.setCharacterSize(32);
    
    text1_.setPosition(16, window.getSize().y/2 -
        text1_.getGlobalBounds().height/2);
    
    text2_.setString("Tenebroso");
    text2_.setPosition(window.getSize().x - text2_.getGlobalBounds().width - 16,
        window.getSize().y/2 - text2_.getGlobalBounds().height/2);
    
    sample_image_.setPosition(window.getSize().x/2 -
        sample_image_.getGlobalBounds().width/2, window.getSize().y/2 - 
        sample_image_.getGlobalBounds().height/2);
}

TutorialStage::~TutorialStage()
{
}

void TutorialStage::handle_events(sf::Event& event)
{
    switch(event.type)
    {
        case sf::Event::Closed:
            window_.close();
            finished_ = true;
            break;
        case sf::Event::KeyReleased:
            if(state_control_ == EXPL_TEXT)
            {
                ++state_control_;
                prsnt_clock_.restart();
            }
            else if(event.key.code == sf::Keyboard::A)
            {
                choice_ = LEFT;
                feedback_ = true;
                ++state_control_;
            }
            else if(event.key.code == sf::Keyboard::L)
            {
                choice_ = RIGHT;
                feedback_ = true;
                ++state_control_;
            }
            if(event.key.code == sf::Keyboard::Escape){
                finished_ = true;
            }
            break;
        default:
            //do nothing
            break;
    }
}

void TutorialStage::update()
{
    if(!window_.isOpen()){
        finished_ = true;
        return;
    }
    
    window_.clear({60, 60, 60, 255});
    
    if(feedback_)
    {
        if(++feedback_frames_ < 24)
        {
            sf::Color alpha(255, 255, 255, 255 - (uint8)feedback_frames_*10);
            if(choice_ == LEFT){
                text1_.setFillColor(alpha);
            }
            else if(choice_ == RIGHT){
                text2_.setFillColor(alpha);
            }
            window_.draw(text1_);
            window_.draw(text2_);
            window_.display();
        }
        else{
            feedback_ = false;
            feedback_frames_ = 0;
        }
        return;
    }
    
    switch(state_control_)
    {
        case States::EXPL_TEXT:
            window_.draw(text1_);
            break;
        case States::IMG_PRSNT:
            if(prsnt_clock_.getElapsedTime().asSeconds() > 2.5f){
                text1_.setString("Sereno");
                ++state_control_;
                break;
            }
            window_.draw(sample_image_);
            break;
        case States::ADJ_PAIR1:
            text1_.setString("Inquietante");
            text2_.setString("Sereno");
            update_texts();
            window_.draw(text1_);
            window_.draw(text2_);
            break;
        case States::ADJ_PAIR2:
            text1_.setString("Repulsivo");
            text2_.setString(L"Aconchegante");
            update_texts();
            window_.draw(text1_);
            window_.draw(text2_);
            break;
        case States::ADJ_PAIR3:
            text1_.setString(L"Tenebroso");
            text2_.setString(L"Repulsivo");
            update_texts();
            window_.draw(text1_);
            window_.draw(text2_);
            break;
        case States::ADJ_PAIR4:
            text1_.setString("Inquietante");
            text2_.setString("Repulsivo");
            update_texts();
            window_.draw(text1_);
            window_.draw(text2_);
            break;
        case States::TUT_END:
            if(++end_frames_ < 120){
                sf::Color alpha(255, 255, 255, 255 - (uint8)end_frames_*2);
                text1_.setString("Fim do Tutorial.");
                text1_.setPosition(window_.getSize().x/2 - text1_.getGlobalBounds()
                    .width/2, window_.getSize().y/2 - text1_.getGlobalBounds().height);
                text1_.setFillColor(alpha);
                    
                window_.draw(text1_);
            }
            else{
                finished_ = true;
            }
            break;
        default:
            break;
    }
    
    window_.display();
}

void TutorialStage::update_texts()
{
    text1_.setPosition(64, window_.getSize().y/2 -
    text1_.getGlobalBounds().height/2);

    text2_.setPosition(window_.getSize().x - 
    text2_.getGlobalBounds().width - 64,
        window_.getSize().y/2 - text2_.getGlobalBounds().height/2);
        
    text1_.setFillColor({255,255,255,255});
    text2_.setFillColor({255,255,255,255});
}

}
    
}

