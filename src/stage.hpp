#ifndef STATE_HPP
#define STATE_HPP

#include <SFML/Graphics.hpp>

namespace xaos
{

namespace neolucid
{

class Stage
{
protected:
    
    sf::RenderWindow&   window_;
    bool                finished_;

public:

    Stage(sf::RenderWindow& window)
    : window_(window), finished_(false) {}
    
    virtual ~Stage() {}

    virtual void update() = 0;
    
    virtual void handle_events(sf::Event&) = 0;
    
    void poll_events() {
        if(!window_.isOpen()){
            finished_ = true;
            return;
        }
        
        sf::Event e;
        while(window_.pollEvent(e)){
            handle_events(e);
        }
    }
    
    bool finished() const {
        return finished_;
    }
    
    static sf::Font& get_font() {
        static bool font_loaded = false;
        static sf::Font font;
        
        if(!font_loaded){
            font.loadFromFile("res/font.ttf");
            font_loaded = true;
        }
        
        return font;
    }

};

}

}

#endif // STATE_HPP
