#include "intro_stage.hpp"

namespace xaos
{

namespace neolucid
{

IntroStage::IntroStage(sf::RenderWindow& window)
: Stage(window),
  frame_counter(0)
{    
    text.setFont(get_font());
    text.setString("Lucid");
    text.setPosition(window.getSize().x/2 - text.getGlobalBounds().width/2,
        window.getSize().y/2 - text.getGlobalBounds().height/2);
    text.setFillColor({255,255,255,0});
}

IntroStage::~IntroStage()
{
}

void IntroStage::handle_events(sf::Event& event)
{
    if(event.type == sf::Event::Closed){
        window_.close();
        finished_ = true;
    }
    if(event.type == sf::Event::KeyReleased && event.key.code == 
        sf::Keyboard::Escape)
    {
        finished_ = true;
    }
}

void IntroStage::update()
{
    if(!window_.isOpen()){
        finished_ = true;
        return;
    }
    
    if(++frame_counter < 256){
        text.setFillColor({255, 255, 255, (sf::Uint8)frame_counter});
    }
    else{
        finished_ = true;
    }
    
    window_.clear({60, 60, 60, 255});
    window_.draw(text);
    window_.display();
}

}

}
