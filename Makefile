.PHONY: clean All

All:
	@echo "----------Building project:[ NeoLucid_Windows32 - Release ]----------"
	@"$(MAKE)" -f  "NeoLucid_Windows32.mk"
clean:
	@echo "----------Cleaning project:[ NeoLucid_Windows32 - Release ]----------"
	@"$(MAKE)" -f  "NeoLucid_Windows32.mk" clean
